(function($) {

var main = function() {
  $(".gallery-image").fancybox({
    afterShow: onSwipe
    });
}

var onSwipe = function() {
  if ('ontouchstart' in document.documentElement) {
    $('.fancybox-nav').css('display', 'none');
    $('.fancybox-wrap').swipe({
      swipe : function(event, direction) {
        if (direction === 'left' || direction === 'up') {
          $.fancybox.prev(direction);
        } else {
          $.fancybox.next(direction);
        }
      }});
  }
};

$(document).ready(main); 

})(jQuery);
